# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-16 20:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionario', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='funcionario',
            name='data_entrada',
        ),
        migrations.RemoveField(
            model_name='funcionario',
            name='data_saida',
        ),
    ]
