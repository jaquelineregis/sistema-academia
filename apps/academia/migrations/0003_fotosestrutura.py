# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-17 00:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('academia', '0002_auto_20170516_2042'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotosEstrutura',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', models.ImageField(upload_to='estrutura')),
                ('titulo', models.CharField(max_length=50, verbose_name='Titulo')),
                ('descricao', models.CharField(max_length=50, verbose_name='Descricao')),
            ],
        ),
    ]
