from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from apps.funcionario.models import *
from apps.academia.models import *
from .forms import *


# Create your views here.
def index(request):
    funcionarios = Funcionario.objects.all()
    estruturas = FotosEstrutura.objects.all()
    precos = Precos.objects.all()
    servicos = Servicos.objects.all()
    context = {
        'funcionario':funcionarios,
        'estruturas':estruturas,
        'precos': precos,
        'servicos': servicos,
    }
    return render(request, 'academia/index.html', context)

@login_required
def base(request):
    return render(request, 'academia/cliente.html', {})


def login(request):
    entrar = Logar(request.POST or None)
    context = {'entrar': entrar,}
    if entrar.is_valid():
        form_dado = entrar.cleaned_data
        loginForm = form_dado['login']
        senhaForm = form_dado['senha']

        if loginForm == 'admin' and senhaForm == 'admin123':
            ok = True
            context['ok'] = ok
            return render(request, 'academia/ponto.html', context)
    return render(request, 'academia/login.html', context)
