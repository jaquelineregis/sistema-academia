from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Pessoa(models.Model):
    cpf = models.CharField(primary_key=True, max_length=14, verbose_name=_('Pessoa - CPF'))
    #data_nasc = models.DateField(verbose_name=_('Data de Nascimento'))
    #endereco = models.CharField(max_length=200, verbose_name=_('Endereço'))
    #telefone = models.CharField(max_length=16, verbose_name=_('Telefone'))
    #celular = models.CharField(max_length=16, verbose_name=_('Celular'))
    foto = models.ImageField(upload_to='funcionario')
    user = models.ForeignKey(User)

    def __str__(self):
        return str(self.user)

class FotosEstrutura(models.Model):
    foto = models.ImageField(upload_to='Foto')
    titulo = models.CharField(max_length=50, verbose_name='Titulo')
    descricao = models.CharField(max_length=50, verbose_name='Descricao')

    def __str__(self):
       return str(self.titulo)

class Precos(models.Model):
    tipo = models.CharField(max_length=50, verbose_name='Tipo')
    instrutor = models.CharField(max_length=50, verbose_name='Instrutos')
    faltas = models.CharField(max_length=50, verbose_name='Faltas')
    idas = models.CharField(max_length=50, verbose_name='Idas')
    horario = models.CharField(max_length=50, verbose_name='Horario')
    valor = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Valor')

    def __str__(self):
       return str(self.tipo)

class Servicos(models.Model):
    foto = models.ImageField(upload_to='Foto')
    titulo = models.CharField(max_length=50, verbose_name='Titulo')
    descricao = models.CharField(max_length=50, verbose_name='Descricao')

    def __str__(self):
       return str(self.titulo)