from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Pessoa)
admin.site.register(FotosEstrutura)
admin.site.register(Precos)
admin.site.register(Servicos)
