import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from apps.academia.models import Pessoa


class Cliente(models.Model):
    mensalidade = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Mensalidade')
    dia_mensalidade = models.IntegerField(verbose_name='Dia da Mensalidade')
    pessoa = models.ForeignKey(Pessoa)


    def __str__(self):
        return str(self.pessoa)


class ReceberMensalidade(models.Model):
    data_recebimento = models.DateField(verbose_name='Data de Recebimento')
    cliente = models.ForeignKey(Cliente, unique_for_month='data_recebimento', verbose_name='CPF',
                                on_delete=models.PROTECT)
    confirmacao = models.BooleanField()

    def __str__(self):
        return str(self.cliente)

class PontoCliente(models.Model):
    data_entrada = models.DateField(default=datetime.date.today, verbose_name=_('Data de Entrada'))
    data_saida = models.DateField(blank=True, null=True, verbose_name=_('Data de Saída'))
    cliente = models.ForeignKey(Cliente, unique_for_date='data_entrada', on_delete=models.PROTECT)

    def __str__(self):
        return str(self.cliente)